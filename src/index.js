import './style.css';
import 'jquery';
var moment = require('moment');
import 'moment-countdown';
import 'moment-timezone';
import 'timezone-picker'

function highlightMap(value) {
  var elements = $('#map').find('svg polygon').filter(function() {
    return $(this).attr("data-offset") >= value;
  })
  elements.attr('data-selected', 'true');
  elements.prop('selected', true);
}

function leftPad(number) {
  if(number < 10) {
    return "0" + number;
  } else {
    return "" + number;
  }
}

function setTimer() {
  var endOfDay = moment().endOf('day').countdown();
  var hoursLeft = leftPad(endOfDay.hours);
  var minutesLeft = leftPad(endOfDay.minutes);
  var secondsLeft = leftPad(endOfDay.seconds);
  var height = $(window).height()/6
  $('#countdown').css('font-size', height)
  $('#countdown').html("" + hoursLeft + ":" + minutesLeft + ":" + secondsLeft);
}

function updateCountdown() {
  var date = new Date();
  var offset = 24 - date.getHours() - date.getTimezoneOffset()/60;
  setTimer(date);
  highlightMap(offset);
}

$('#map').timezonePicker();
$('.Cbox').hide();
updateCountdown();
setInterval(updateCountdown, 200);
